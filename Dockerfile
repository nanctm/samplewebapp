FROM maven:3.5-jdk-8 as BUILD
COPY src /usr/src/myapp/src
COPY pom.xml /usr/src/myapp
RUN mvn -f /usr/src/myapp/pom.xml clean package

FROM tomcat:9.0.1-jre8-alpine
COPY --from=BUILD /usr/src/myapp/target/SampleWebApp.war /usr/local/tomcat/webapps/samplewebapp.war
#ENV TZ=America/Los_Angeles
#EXPOSE 8080
CMD ["catalina.sh", "run"]
EXPOSE 8888/tcp



# FROM tomcat:9.0.1-jre8-alpine

# ADD ./target/SampleWebApp.war /usr/local/tomcat/webapps/samplewebapp.war

# CMD ["catalina.sh", "run"]

# EXPOSE 8888/tcp
